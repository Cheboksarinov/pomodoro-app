﻿#region Usings

using ViewModel;

#endregion

namespace PomodoroTests.Fakes {
    public class DataStorageFake : IDataStorage {
        public string SavedData { get; private set; }

        public void SetDataInStorage(string data) {
            SavedData = data;
        }

        public string LoadDataFromStorage() {
            return  SavedData;
        }
    }
}