﻿#region Usings

using Domain;
using NUnit.Framework;
using PomodoroTests.Fakes;
using ViewModel;

#endregion

namespace PomodoroTests {
    [TestFixture]
    public class TaskBarViewModelTest {
        private FakeTimer fakeTimer;
        private TaskBarViewModel taskBarViewModel;
        private JsonFileDataManager testDataManager;
        private DataStorageFake testDataStorage;
        private PomodoroTimerValues testModel;
        private SessionManager testSessionManager;
        private SessionInfoViewModel testSessionInfoViewModel;

        [SetUp]
        public void SetUp() {
            testModel = new PomodoroTimerValues();
            fakeTimer = new FakeTimer();
            testDataStorage = new DataStorageFake();
            testDataManager = new JsonFileDataManager(testDataStorage);
            testSessionManager = new SessionManager(fakeTimer, testDataManager);
            taskBarViewModel = new TaskBarViewModel(testSessionManager, testModel);
            testSessionInfoViewModel = new SessionInfoViewModel(testSessionManager);
        }

        [Test]
        public void MinusOneSecondInViewModel_Tick_TickOneSecond() {
            var secondValueBeforeTick = taskBarViewModel.SecondValue;

            fakeTimer.ThrowTick(1);
            var secondValueAfterTick = taskBarViewModel.SecondValue;

            Assert.AreEqual(secondValueBeforeTick - 1, secondValueAfterTick);
        }

        [Test]
        public void StatusChange_Tick_PomodoroTimeIsOver() {
            testSessionInfoViewModel.RunTimer.Execute(null);
            CompletePomodoro();

            fakeTimer.ThrowTick(1);
            var statusAfterPomodoroIsOver = taskBarViewModel.CurrentStatus;

            Assert.AreEqual("Rest", statusAfterPomodoroIsOver);
        }

        [Test]
        public void BalloonTextChangeOnRest_Tick_PomodoroTimeIsOver() {
            testSessionInfoViewModel.RunTimer.Execute(null);
            
            CompletePomodoro();
            fakeTimer.ThrowTick(1);
            var ballonTextAfterPomodoro = taskBarViewModel.BalloonText;

            Assert.AreEqual("Pomodoro is over. Get a rest", ballonTextAfterPomodoro);
        }

        [Test]
        public void BalloonTextChangeOnWaiting_Tick_RestTimeIsOver()
        {
            testSessionInfoViewModel.RunTimer.Execute(null);
            CompletePomodoro();
            fakeTimer.ThrowTick(1);
            CompleteRest();

            var ballonTextAfterPomodoro = taskBarViewModel.BalloonText;
            Assert.AreEqual("Rest time is over. Start new pomodoro?", ballonTextAfterPomodoro);
        }

        private void CompleteRest() {
            fakeTimer.ThrowTick(5);
        }

        [Test]
        public void UiStringChange_Tick_TimeChanged() {
            var secondValueBeforeTick = taskBarViewModel.SecondValue;

            fakeTimer.ThrowTick(1);
            var uiStringAfterSecondTicked = taskBarViewModel.UiString;
            Assert.AreEqual($"CurrentStatus: {taskBarViewModel.CurrentStatus}\n Time left {taskBarViewModel.MinuteValue}:{secondValueBeforeTick - 1}",
                uiStringAfterSecondTicked);
        }


        private void CompletePomodoro() {
            fakeTimer.ThrowTick(10);
        }

        private void TickOneMinute() {
            fakeTimer.ThrowTick(60);
        }
    }
}