﻿#region Usings

using System;
using System.Linq;
using Domain;
using Newtonsoft.Json;
using NUnit.Framework;
using PomodoroTests.Fakes;
using ViewModel;

#endregion

namespace PomodoroTests {
    [TestFixture]
    public class SessionDataManagerTests {
        private JsonFileDataManager dataManager;
        private DataStorageFake dataStorage;
        private DateTime today;
        private DateTime yesterday;

        [SetUp]
        public void Setup() {
            dataStorage = new DataStorageFake();
            dataManager = new JsonFileDataManager(dataStorage);

            today = DateTime.Today;
            yesterday = DateTime.Today.AddDays(-1);
        }

        [Test]
        public void CorrectDataFormat_SaveSessionJson_SessionInfoInNotNull() {
            var pomodoro3 = new DailyActivity {PomodoroCount = 3};
            dataManager.SavePomodoro(pomodoro3);

            var expectedJson = JsonConvert.SerializeObject(new[] {new DailyActivitySaveData(pomodoro3)});
            var result = dataStorage.SavedData;
            Assert.AreEqual(expectedJson, result);
        }

        [Test]
        public void LoadCorrectData_LoadSessionFromJson_DataStorageIsCorrect() {
            var pomodoro3 = new DailyActivity {PomodoroCount = 3};
            dataStorage.SetDataInStorage(JsonConvert.SerializeObject(new[] {new DailyActivitySaveData(pomodoro3)}));

            var result = dataManager.GetTodayPomodoroNumber();

            Assert.AreEqual(3, result.PomodoroCount);
        }

        [Test]
        public void KeepYesterdayAndTodayPomodoroSessionInfo_SaveTodayPomodoro_YesterdayPomodoroExists() {
            var yesterdayPomodoro = new DailyActivity();
            yesterdayPomodoro.PomodoroCount = 7;
            yesterdayPomodoro.Date = yesterday;
            var todayPomodoro = new DailyActivity();
            todayPomodoro.PomodoroCount = 3;
            todayPomodoro.Date = today;

            dataManager.SavePomodoro(yesterdayPomodoro);
            dataManager.SavePomodoro(todayPomodoro);

            var expectedJson = JsonConvert.SerializeObject(new[] {new DailyActivitySaveData(yesterdayPomodoro), new DailyActivitySaveData(todayPomodoro), });
            var result = dataStorage.SavedData;
            Assert.AreEqual(expectedJson, result);
        }

        [Test]
        public void TodayCompleted3Pomodoros_GetTodayPomodoro_HasYesterdayAndTodayPomodoros() {
            var yesterdayPomodoro = new DailyActivity {PomodoroCount = 7, Date = yesterday};
            var todayPomodoro = new DailyActivity {PomodoroCount = 3, Date = today};
            SavePomodoro(yesterdayPomodoro, todayPomodoro);

            var restoredTodayPomodoro = dataManager.GetTodayPomodoroNumber();

            Assert.AreEqual(3, restoredTodayPomodoro.PomodoroCount);
        }

        [Test]
        public void SaveAllTodayPomodoroInOneRecord_SavePomodoro_TodayPomodoroExists() {
            var today3Pomodoro = new DailyActivity {PomodoroCount = 3, Date = today};
            var today5Pomodoro = new DailyActivity {PomodoroCount = 5, Date = today};

            dataManager.SavePomodoro(today3Pomodoro);
            dataManager.SavePomodoro(today5Pomodoro);

            var allPomodoras = JsonConvert.DeserializeObject<DailyActivity[]>(dataStorage.SavedData);
            var todayPomodorosCount = allPomodoras.Count(_ => _.Date == DateTime.Today);
            Assert.AreEqual(1, todayPomodorosCount);
        }


        private void SavePomodoro(params DailyActivity[] pomodoros) {
            var pomodorosToSave = pomodoros.Select(pomodoro => new DailyActivitySaveData(pomodoro)).ToList();
            dataStorage.SetDataInStorage(JsonConvert.SerializeObject(pomodorosToSave));
        }
    }
}