﻿#region Usings

using System;
using System.Collections.Generic;
using Domain;
using NUnit.Framework;
using PomodoroTests.Fakes;
using ViewModel;

#endregion

namespace PomodoroTests {
    [TestFixture]
    public class SessionInfoViewModelTests {
        private SessionInfoViewModel testSessionInfoViewModel;
        private SessionManager testSessionManager;
        private FakeTimer testTimer;
        private DataStorageFake testDataStorage;
        private JsonFileDataManager testDataManager;

        [SetUp]
        public void SetUp() {
            testTimer = new FakeTimer();
            testDataStorage = new DataStorageFake();
            testDataManager = new JsonFileDataManager(testDataStorage);
            testSessionManager = new SessionManager(testTimer, testDataManager);
            testSessionInfoViewModel = new SessionInfoViewModel(testSessionManager);
        }

        [Test]
        public void SetActivityDescription_StartNewPomodoro_DescriptionIsNotEmpty() {
            var inputedDescription = "Testing pomodoro app";

            testSessionManager.StartNewPomodoro(inputedDescription);

            Assert.AreEqual(inputedDescription, testSessionInfoViewModel.CurrentActivityDescription);
        }

        [Test]
        public void TwoPomodorasForActivity_PomodoroIsStarted_OnePomodoroWithThisDescriptionExists() {
            var activityDescription = "Planned two pomodoras on this activity";
            AddSessionWithOneActivity(activityDescription);

            testSessionManager.StartNewPomodoro(activityDescription);

            Assert.AreEqual(2, testSessionManager.GetSessionInfo().GetPomodoroNumbers(activityDescription));
        }

        private void AddSessionWithOneActivity(string activityDescription) {
            var todaySession = new DailyActivity();
            todaySession.AddActivity(activityDescription,1);
            todaySession.PomodoroCount = 1;
            todaySession.Date = DateTime.Today;
            testDataManager.SavePomodoro(todaySession);
        }
    }
}