﻿#region Usings

using System;
using Domain;
using Newtonsoft.Json;
using NUnit.Framework;
using PomodoroTests.Fakes;
using ViewModel;

#endregion

namespace PomodoroTests {
    [TestFixture]
    public class SessionTimeManagerTests {
        private SessionManager sessionManager;
        private FakeTimer fakeTimer;
        private JsonFileDataManager dataManager;
        private DataStorageFake dataStorage;
        private SessionInfoViewModel testSessionInfoViewModel;
        [SetUp]
        public void SetUp() {
            fakeTimer = new FakeTimer();
            dataStorage = new DataStorageFake();
            dataManager = new JsonFileDataManager(dataStorage);
            sessionManager = new SessionManager(fakeTimer, dataManager);
            testSessionInfoViewModel = new SessionInfoViewModel(sessionManager);
        }


        [Test]
        public void ReduceSecondByOne_TimerTick_TimerTickCorrect() {
            var activity = "test pomodoro";
            sessionManager.StartNewPomodoro(activity);

            fakeTimer.ThrowTick(1);
            Assert.That(sessionManager.GetSecond(), Is.EqualTo(9));
        }

        [Test]
        public void StopTimerAfterOverRestTimer_StopRestTime_ResTimeIsOver() {
            CompletePomodoro();
            CompleteRest();
            fakeTimer.ThrowTick(1);

            Assert.That(sessionManager.IsSessionOver() && sessionManager.GetMinute() == 0 && sessionManager.GetSecond() == 0,
                Is.True);
        }

        [Test]
        public void NewDayPomodoroAreLoadSuccsessful_LoadTodayPomodoroNumber_TommorowPomodoroIsZero() {
            var activity = "test pomodoro";

            sessionManager.StartNewPomodoro(activity);
            Assert.That(sessionManager.GetSessionInfo().PomodoroCount, Is.EqualTo(0));
        }

        [Test]
        public void StartNewPomodoroIfStatusWaiting_ThrowOneTick_StatusIsWaiting() {
            testSessionInfoViewModel.RunTimer.Execute(null);
            fakeTimer.ThrowTick(1);

            Assert.AreEqual("Pomodoro", sessionManager.GetStatus());
        }

        [Test]
        public void StartRestIfPomodoroOver_ThrowPomodoroCountTicks_StatusIsWaiting() {
            testSessionInfoViewModel.RunTimer.Execute(null);
            CompletePomodoro();

            fakeTimer.ThrowTick(1);
            Assert.AreEqual("Rest", sessionManager.GetStatus());
        }

        [Test]
        public void WaitingIfRestIsOver_ThrowPomodoroAndRestCountTicks_StatusIsWaiting() {
            CompletePomodoro();
            CompleteRest();
            fakeTimer.ThrowTick(1);

            Assert.AreEqual("Waiting", sessionManager.GetStatus());
        }

        [Test]
        public void BigBreakAfterFourPomodoro_StartPomodoro_FourPomodorosIsOver() {
            var dailyActivity = new DailyActivity {PomodoroCount = 3, Date = DateTime.Today};
            var pomodoroThreeSession = new [] {new DailyActivitySaveData(dailyActivity)};

            dataStorage.SetDataInStorage(JsonConvert.SerializeObject(pomodoroThreeSession));

            testSessionInfoViewModel.RunTimer.Execute(null);
            CompletePomodoro();
            fakeTimer.ThrowTick(1);

            Assert.AreEqual("Big break", sessionManager.GetStatus());
        }

        private void CompleteRest() {
            fakeTimer.ThrowTick(5);
        }

        private void CompletePomodoro() {
            fakeTimer.ThrowTick(10);
        }
    }
}