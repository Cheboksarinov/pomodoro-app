﻿#region Usings

using System;
using System.Runtime.Remoting.Channels;
using ViewModel;

#endregion

namespace PomodoroTests {
    public class FakeTimer : ITimer {
        public  void Start() {
            
        }

        public event EventHandler Tick;

        public  void Stop() {
           
        }

        public void ThrowTick(int tickCount) {
            for (var countIndex = 0; countIndex < tickCount; countIndex++) {
                Tick?.Invoke(this, new EventArgs());
            }
        }
    }
}