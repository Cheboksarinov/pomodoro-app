﻿#region Usings

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Model {
    public class DailyActivity {
        public DailyActivity() {
            Date = DateTime.Today;
            pomodoros = new Dictionary<string, int>();
        }

        public int PomodoroCount { get; set; }
        public DateTime Date { get; set; }
        private readonly Dictionary<string, int> pomodoros;

        public void Increment(string description) {
            if (pomodoros.ContainsKey(description)) {
                pomodoros[description] += 1;
            }
            else {
                pomodoros.Add(description, 1);
            }
        }

        public string GetLastActivtyDescription() {
            return pomodoros.Count != 0 ? pomodoros.Keys.Last() : "";
        }

        public int GetPomodoroNumbers(string activityDescription) {
            return pomodoros[activityDescription];
        }

        public void AddActivity(string activityDescription, int pomodorosNumber) {
            pomodoros.Add(activityDescription, pomodorosNumber);
        }

        public Dictionary<string, int> GetPomodoros() {
            return pomodoros;
        }
    }
}