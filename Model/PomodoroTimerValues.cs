﻿namespace Model {

    public class PomodoroTimerValues
    {
        public int TimerMinute { get; set; }
        public int TimerSec { get; set; }

        public string Status { get; set; }
    }
}