﻿#region Usings

using System;
using System.Windows;
using ViewModel;

#endregion

namespace PomodoroView {
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {


        
        public TimerViewModel ViewModel
        {
            set { DataContext = value; }
        }

        public MainWindow() {
            InitializeComponent();
          
        }
    }
}