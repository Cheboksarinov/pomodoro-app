﻿#region Usings

using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Hardcodet.Wpf.TaskbarNotification;
using ViewModel;

#endregion

namespace PomodoroApp {
    public class TaskBarWatcher {
        private TaskbarIcon appTaskbarIcon;
        private readonly SessionManager sessionManager;
        private readonly MainViewModel mainViewModel;
        private StatusBaloon statusBaloon;

        public TaskBarWatcher(SessionManager sessionManager, MainViewModel mainViewModel) {
            this.sessionManager = sessionManager;
            this.mainViewModel = mainViewModel;
            sessionManager.OnStatusChanged += SessionTimeManager_OnStatusChanged;
        }

        public void ConnectToTaskBarIcon(TaskbarIcon appTaskBarIcon) {
            appTaskbarIcon = appTaskBarIcon;
            appTaskbarIcon.TrayMouseDoubleClick += PomodoroTaskBarIcon_TrayMouseDoubleClick;
        }

        private void SessionTimeManager_OnStatusChanged() {
            if (appTaskbarIcon.Visibility == Visibility.Visible) {
                if (sessionManager.GetStatus() == "Rest" || sessionManager.GetStatus() == "Big break") {
                    CreateStatusBaloon();
                    appTaskbarIcon.ShowCustomBalloon(statusBaloon, PopupAnimation.Scroll, 4000);
                    SetMainWindoNormal();
                }
                if (sessionManager.GetStatus() == "Waiting") {
                    CreateStatusBaloon();
                    appTaskbarIcon.ShowCustomBalloon(statusBaloon, PopupAnimation.Scroll, 4000);
                }
            }
        }

        private void CreateStatusBaloon() {
            statusBaloon = new StatusBaloon();
            statusBaloon.DataContext = mainViewModel;
            statusBaloon.MouseLeftButtonDown += StatusBaloon_MouseLeftButtonDown;
        }

        private void StatusBaloon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            SetMainWindoNormal();
        }

        private void PomodoroTaskBarIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e) {
            SetMainWindoNormal();
        }

        private void SetMainWindoNormal() {
            Application.Current.MainWindow.Visibility = Visibility.Visible;
            Application.Current.MainWindow.Hide();
            Application.Current.MainWindow.Show();
            Application.Current.MainWindow.WindowState = WindowState.Normal;
        }
    }
}