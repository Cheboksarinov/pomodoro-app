﻿#region Usings

using System;
using System.Windows;
using System.Windows.Threading;
using WindowsServices;
using Domain;
using Hardcodet.Wpf.TaskbarNotification;
using PomodoroApp;
using ViewModel;

#endregion

namespace PomodoroView {
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        private SessionManager timeManager;
        private TaskbarIcon pomodoroTaskBarIcon;
        private PomodoroTimerValues pomodoroTimeData;
        private TaskBarWatcher taskBarWatcher;
        private MainViewModel mainViewModel;
        private StatusBaloon statusBaloon;
        protected override void OnStartup(StartupEventArgs e) {
            try {
                
                timeManager = new SessionManager(new TimerInApp(), new JsonFileDataManager(new FileDataStorage()));
                pomodoroTimeData = new PomodoroTimerValues();
                mainViewModel = new MainViewModel(timeManager, pomodoroTimeData);
                InitializeTaskBarServices();
                var view = new MainWindow {
                    DataContext = mainViewModel
                };
                view.StateChanged += View_StateChanged;
                view.Show();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString(), "Exception");
            }
        }

        private void OnApplicationException(object sender, DispatcherUnhandledExceptionEventArgs e) {
#if DEBUG
            e.Handled = false;
#else
         MessageBox.Show(e.Exception.ToString(), "Exception");
#endif

        }

        private void OnException(object sender, UnhandledExceptionEventArgs e) {
#if DEBUG            
#else
         MessageBox.Show(e.ExceptionObject.ToString(), "Exception");
#endif
        }

        private void InitializeTaskBarServices() {
            pomodoroTaskBarIcon = (TaskbarIcon) Resources["PomodoroIcon"];
            pomodoroTaskBarIcon.DataContext = mainViewModel;
            statusBaloon = new StatusBaloon();
            taskBarWatcher = new TaskBarWatcher(timeManager, mainViewModel);
            taskBarWatcher.ConnectToTaskBarIcon(pomodoroTaskBarIcon);
            pomodoroTaskBarIcon.Visibility = Visibility.Collapsed;
        }


        private void View_StateChanged(object sender, EventArgs e) {
            if (MainWindow.WindowState == WindowState.Minimized) {
                pomodoroTaskBarIcon.Visibility = Visibility.Visible;
                MainWindow.Visibility = Visibility.Hidden;
            }
            if (MainWindow.WindowState != WindowState.Normal) return;
            pomodoroTaskBarIcon.Visibility = Visibility.Collapsed;
            pomodoroTaskBarIcon.HideBalloonTip();
        }        
    }
}