#region Usings

using System;
using System.Windows.Input;

#endregion

namespace ViewModel {
    public class Command : ICommand {
        private readonly Action action;
        private readonly Func<bool> canExecute;

        public Command(Action action, Func<bool> canExecute = null) {
            this.action = action;
            this.canExecute = canExecute ?? (() => true);
        }

        public bool CanExecute(object parameter) {
            return canExecute();
        }

        public void Execute(object parameter) {
            action();
        }

        public event EventHandler CanExecuteChanged;
    }
}