﻿#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Newtonsoft.Json;

#endregion

namespace ViewModel {
    public class JsonFileDataManager : ISessionDataManager {
        private readonly IDataStorage dataStorage;

        public JsonFileDataManager(IDataStorage dataStorage) {
            this.dataStorage = dataStorage;
        }

        public bool SavePomodoro(DailyActivity session) {
            var pomodoros = new List<DailyActivity>(GetAllPomodoros());

            var todayPomodoro = pomodoros.FirstOrDefault(_ => _.Date == DateTime.Today);
            if (todayPomodoro == null) {
                pomodoros.Add(session);
            }
            else {
                pomodoros.Remove(todayPomodoro);
                pomodoros.Add(session);
            }
            var pomodorosToSave = pomodoros.Select(pomodoro => new DailyActivitySaveData(pomodoro)).ToList();
            var json = JsonConvert.SerializeObject(pomodorosToSave);
            dataStorage.SetDataInStorage(json);
            return true;
        }

        public DailyActivity GetTodayPomodoroNumber() {
            var allPomodoros = GetAllPomodoros();
            var todayPomodoro = allPomodoros.SingleOrDefault(_ => _.Date == DateTime.Today);
            return todayPomodoro ?? new DailyActivity();
        }

        private DailyActivity[] GetAllPomodoros() {
            var pomodorosJson = dataStorage.LoadDataFromStorage();
            if (pomodorosJson == null)
                return new DailyActivity[0];

            var loadedData = JsonConvert.DeserializeObject<DailyActivitySaveData[]>(pomodorosJson);
            if (loadedData == null) {
                return new DailyActivity[0];
            }
            var dailyActivities = GetDailyActivityFromLoadedData(loadedData);
            return dailyActivities;
        }

        private DailyActivity[] GetDailyActivityFromLoadedData(DailyActivitySaveData[] loadedData) {
            var dailyActivities = new DailyActivity[loadedData.Length];
            for (var index = 0; index < loadedData.Length; index++) {
                dailyActivities[index] = new DailyActivity();
                dailyActivities[index].PomodoroCount = loadedData[index].pomodorosCount;
                dailyActivities[index].Date = loadedData[index].date;
                foreach (var pomodoro in loadedData[index].pomodoros) {
                    dailyActivities[index].AddActivity(pomodoro.Key, pomodoro.Value);
                }
            }
            return dailyActivities;
        }
    }
}