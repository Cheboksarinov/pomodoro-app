﻿#region Usings

using System;

#endregion

namespace ViewModel {
    public interface ITimer {
        void Start();

        event EventHandler Tick;

        void Stop();
    }
}