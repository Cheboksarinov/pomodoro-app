﻿#region Usings

using System;
using System.Collections.Generic;
using Domain;

#endregion

namespace ViewModel {
    public class DailyActivitySaveData {
        private readonly DailyActivity dailyActivity;

        public Dictionary<string, int> pomodoros;
        public int pomodorosCount;
        public DateTime date;
        public DailyActivitySaveData(DailyActivity dailyActivity) {
            this.dailyActivity = dailyActivity;
            pomodoros = dailyActivity.GetPomodoros();
            pomodorosCount = dailyActivity.PomodoroCount;
            date = dailyActivity.Date;
        }

        public DailyActivitySaveData() {
            
        }
    }
}