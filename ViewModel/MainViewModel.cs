﻿#region Usings

using Domain;
using Hardcodet.Wpf.TaskbarNotification;

#endregion

namespace ViewModel {
    public class MainViewModel {
        public TimerViewModel timerViewModel { get; private set; }
        public SessionInfoViewModel sessionInfoViewModel { get; private set; }
        public TaskBarViewModel taskBarViewModel { get; private set; }

        public MainViewModel(SessionManager timeManager, PomodoroTimerValues timeData) {
            timerViewModel = new TimerViewModel(timeManager, timeData);
            sessionInfoViewModel = new SessionInfoViewModel(timeManager);
            taskBarViewModel = new TaskBarViewModel(timeManager, timeData);
        }

        public MainViewModel() {
        }
    }
}