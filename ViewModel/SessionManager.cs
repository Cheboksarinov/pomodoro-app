﻿#region Usings

#endregion

#region Usings

using System;
using Domain;

#endregion

namespace ViewModel {
    public class SessionManager {
        private int secondCounter;
        private int minuteCounter;
        private bool timeOver;
        private string status;
        private readonly ITimer internalTimer;
        private readonly ISessionDataManager internalDataManager;
        private DailyActivity dailyActivity;
#if DEBUG
        public const int PomodoroMinute = 0;
        public const int PomodoroSecond = 10;
        public const int RestMinute = 0;
        public const int RestSecond = 5;
        public const int BigBreakMinute = 0;
        public const int BigBreakSecond = 7;
#else
        public const int PomodoroMinute = 25;
        public const int PomodoroSecond = 0;
        public const int RestMinute = 5;
        public const int RestSecond = 0;
        public const int BigBreakMinute = 30;
        public const int BigBreakSecond = 0;
#endif

        public SessionManager(ITimer internalTimer, ISessionDataManager internalDataManager) {
            this.internalTimer = internalTimer;
            this.internalDataManager = internalDataManager;
            dailyActivity = new DailyActivity();
            SetStatus("Waiting");
            SetTime(PomodoroMinute, PomodoroSecond);
            InitializeSessionInfo();
            internalTimer.Tick += InternalTimer_Tick;
        }

        public delegate void TimeChanged();

        public delegate void StatusChanged();

        public delegate void SessionInfoChanged();

        public event StatusChanged OnStatusChanged;
        public event TimeChanged OnTimeChanged;
        public event SessionInfoChanged OnSessionInfoChanged;


        public void StartNewPomodoro(string activityDescription) {
            SetTime(PomodoroMinute, PomodoroSecond);
            SetStatus("Pomodoro");
            timeOver = false;
            InitializeSessionInfo();
            dailyActivity.Increment(activityDescription);
            OnSessionInfoChanged?.Invoke();
            internalTimer.Start();
        }

        public void Initialize() {
            OnStatusChanged?.Invoke();
            OnTimeChanged?.Invoke();
            OnSessionInfoChanged?.Invoke();
        }

        private void InitializeSessionInfo() {
            dailyActivity = internalDataManager.GetTodayPomodoroNumber();
            OnSessionInfoChanged?.Invoke();
        }


        public int GetMinute() {
            return minuteCounter;
        }

        public int GetSecond() {
            return secondCounter;
        }

        public string GetStatus() {
            return status;
        }

        public DailyActivity GetSessionInfo() {
            return dailyActivity;
        }

        public bool IsSessionOver() {
            return timeOver;
        }

        private void InternalTimer_Tick(object sender, EventArgs e) {
            Tick();
        }

        private void Tick() {
            CalculateNextSecond();
            if (secondCounter >= 0) return;
            SetNextMinute();
            if (minuteCounter >= 0) return;
            if (status == "Pomodoro") {
                StopTimer();
                AddOnePomodoroTodayAndSave();
                SetRestTimer();
                CalculateNextSecond();
                return;
            }
            SetTimeOverMessage();
            StopTimer();
        }

        private void AddOnePomodoroTodayAndSave() {
            dailyActivity.PomodoroCount += 1;
            internalDataManager.SavePomodoro(dailyActivity);
            OnSessionInfoChanged?.Invoke();
        }

        private void StopTimer() {
            internalTimer.Stop();
            timeOver = true;
        }

        private void CalculateNextSecond() {
            SetTime(minuteCounter, secondCounter - 1);
            OnTimeChanged?.Invoke();
        }

        private void SetNextMinute() {
            SetTime(minuteCounter - 1, 59);
        }

        private void SetTime(int minute, int second) {
            minuteCounter = minute;
            secondCounter = second;
            OnTimeChanged?.Invoke();
        }


        private void SetTimeOverMessage() {
            SetTime(0, 0);
            SetStatus("Waiting");
        }


        private void SetRestTimer() {
            if (dailyActivity.PomodoroCount%4 == 0) {
                SetTime(BigBreakMinute, BigBreakSecond);
                SetStatus("Big break");
                internalTimer.Start();
            }
            else {
                SetTime(RestMinute, RestSecond);
                SetStatus("Rest");
                internalTimer.Start();
            }
        }

        private void SetStatus(string inStatus) {
            status = inStatus;
            OnStatusChanged?.Invoke();
        }
    }
}