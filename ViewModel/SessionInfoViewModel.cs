﻿#region Usings

using System.Linq;
using System.Windows.Input;
using Domain;

#endregion

namespace ViewModel {
    public class SessionInfoViewModel : ViewModelBase {
        private readonly SessionManager sessionManager;
        private readonly DailyActivity sessionModel;
        private string currentActivityDescription ;
        public ICommand RunTimer { get; private set; }

        public SessionInfoViewModel(SessionManager sessionManager) {
            this.sessionManager = sessionManager;
            sessionModel = new DailyActivity();
            sessionManager.OnSessionInfoChanged += TimeManager_OnSessionInfoChanged;
            sessionManager.Initialize();
            RunTimer = new Command(OnRunTimer);
        }

        private void TimeManager_OnSessionInfoChanged() {
            NumberOfPomodoroToday = sessionManager.GetSessionInfo().PomodoroCount;
            CurrentActivityDescription = sessionManager.GetSessionInfo().GetLastActivtyDescription();
        }

        public int NumberOfPomodoroToday {
            get { return sessionModel.PomodoroCount; }

            set {
                sessionModel.PomodoroCount = value;
                OnPropertyChanged();
            }
        }

        public string CurrentActivityDescription {
            get { return currentActivityDescription; }
            set {
                currentActivityDescription = value;
                OnPropertyChanged();
            }
        }

        public void OnRunTimer() {
            sessionManager.StartNewPomodoro(CurrentActivityDescription);
        }
    }
}