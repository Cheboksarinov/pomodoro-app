﻿#region Usings

using System;
using Domain;

#endregion

namespace ViewModel {
    public class TimerViewModel : ViewModelBase {
        private readonly PomodoroTimerValues pomodoroTimerValues;
        private readonly SessionManager sessionManager;
        private bool isTimerStarted ;
        public TimerViewModel(SessionManager timeManager, PomodoroTimerValues timeData) {
            pomodoroTimerValues = timeData;
            sessionManager = timeManager;
           

            sessionManager.OnTimeChanged += SessionManagerOnTimeChanged;
            sessionManager.OnStatusChanged += SessionManagerOnStatusChanged;
            sessionManager.Initialize();
        }

     

        public int TimerMinute {
            get { return pomodoroTimerValues.TimerMinute; }

            set {
                pomodoroTimerValues.TimerMinute = Convert.ToInt32(value);
                OnPropertyChanged();
            }
        }

        public bool IsTimerStarted {
            get { return isTimerStarted; }
            set { isTimerStarted = value; OnPropertyChanged(); }
        }
        public int TimerSec {
            get { return pomodoroTimerValues.TimerSec; }

            set {
                pomodoroTimerValues.TimerSec = Convert.ToInt32(value);
                OnPropertyChanged();
            }
        }

        public string Status {
            get { return pomodoroTimerValues.Status; }
            set {
                pomodoroTimerValues.Status = value;
                OnPropertyChanged();
            }
        }

        private void SessionManagerOnStatusChanged() {
            Status = sessionManager.GetStatus();
            IsTimerStarted = IsStatusWaiting();
        }

        private bool IsStatusWaiting() {
            return Status == "Waiting";
        }


        private void SessionManagerOnTimeChanged() {
            TimerMinute = sessionManager.GetMinute();
            TimerSec = sessionManager.GetSecond();
        }
    }
}