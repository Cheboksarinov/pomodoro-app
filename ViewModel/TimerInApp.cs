﻿#region Usings

using System;
using System.Windows.Threading;

#endregion

namespace ViewModel {
    public class TimerInApp : ITimer {
        private readonly DispatcherTimer microsoftTimer;

        public  event EventHandler Tick;

        public TimerInApp() {
            microsoftTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 1)};
            microsoftTimer.Tick += MicrosoftTimer_Tick;
        }

        public  void Stop() {
            microsoftTimer.Stop();
        }

        public  void Start() {
            microsoftTimer.Start();
        }

        private void MicrosoftTimer_Tick(object sender, EventArgs e) {
            Tick?.Invoke(sender, e);
        }
    }
}