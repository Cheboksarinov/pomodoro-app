﻿using Domain;

namespace ViewModel {
    public interface ISessionDataManager {
      
        bool SavePomodoro(DailyActivity session);
         DailyActivity GetTodayPomodoroNumber();

    }
}