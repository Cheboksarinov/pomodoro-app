﻿#region Usings



#endregion

namespace ViewModel {
    public interface IDataStorage {
        void SetDataInStorage(string data);
        string LoadDataFromStorage();
    }
}