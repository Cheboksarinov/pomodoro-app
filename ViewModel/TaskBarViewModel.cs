﻿#region Usings

using System.Reflection;
using System.Windows.Input;
using Domain;

#endregion

namespace ViewModel {
    public class TaskBarViewModel : ViewModelBase {
        private readonly SessionManager sessionManager;
        private readonly PomodoroTimerValues model;
        private string uiString;
        private string balloonText;


        public TaskBarViewModel(SessionManager timeManager, PomodoroTimerValues model) {
            sessionManager = timeManager;
            this.model = model;
            
            sessionManager.OnTimeChanged += SessionManagerOnTimeChanged;
            sessionManager.OnStatusChanged += SessionManagerOnStatusChanged;
            sessionManager.Initialize();
        }

        public string BalloonText  {
            get { return balloonText;}
            set {
                balloonText = value;
                OnPropertyChanged();
            }
        }
        public int MinuteValue {
            get { return model.TimerMinute; }

            set {
                model.TimerMinute = value;
                OnPropertyChanged();
            }
        }

        
        public int SecondValue {
            get { return model.TimerSec; }

            set {
                model.TimerSec = value;
                OnPropertyChanged();
            }
        }

        public string CurrentStatus {
            get { return model.Status; }
            set {
                model.Status = value;
                OnPropertyChanged();
            }
        }

        public string UiString {
            get { return uiString; }
            set {
                uiString = value;
                OnPropertyChanged();
            }
        }

        private void SessionManagerOnStatusChanged() {
            CurrentStatus = sessionManager.GetStatus();
            if (IsBreak()) {
                BalloonText = "Pomodoro is over. Get a rest";
            }
            if (IsBigBreak()) {
                BalloonText = "Four pomodoras completed. Time for big break";
            }
            if (IsWaiting()) {
                BalloonText = "Rest time is over. Start new pomodoro?";
            }
        }

        private bool IsWaiting() {
            return CurrentStatus == "Waiting";
        }

        private void SessionManagerOnTimeChanged() {
            MinuteValue = sessionManager.GetMinute();
            SecondValue = sessionManager.GetSecond();
            UiString = $"CurrentStatus: {CurrentStatus}\n Time left {MinuteValue}:{SecondValue}";
        }


        private bool IsBigBreak()
        {
            return CurrentStatus == "Big break";
        }

        private bool IsBreak()
        {
            return CurrentStatus == "Rest";
        }

    }
}