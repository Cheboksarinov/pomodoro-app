﻿#region Usings

using System;
using System.IO;
using ViewModel;

#endregion

namespace WindowsServices {
    public class FileDataStorage : IDataStorage {
        public void SetDataInStorage(string data) {
            File.WriteAllText(FilePath, data);
        }

        public string LoadDataFromStorage() {
            if (!File.Exists(FilePath)) {
                return "";
            }

            return File.ReadAllText(FilePath);
        }

        private static string FilePath {
            get {
                var pomodoroDirectoryName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PomodoroApp");

                if (!Directory.Exists(pomodoroDirectoryName)) {
                    Directory.CreateDirectory(pomodoroDirectoryName);
                }

                var path = Path.Combine(pomodoroDirectoryName, "session.txt");
                return path;
            }
        }
    }
}